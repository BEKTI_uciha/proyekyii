<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Data */
/* @var $form ActiveForm */
?>
<div class="Data">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'Nama') ?>
        <?= $form->field($model, 'tangga_lahir') ?>
        <?= $form->field($model, 'Tempat_tinggal') ?>
        <?= $form->field($model, 'id') ?>
    
        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- Data -->
