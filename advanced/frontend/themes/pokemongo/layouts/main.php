<?php

use yii\helpers\Html;
use yii\widgets\Menu;
use yii\widgets\Breadcrumbs;

/**
 * @var $this \yii\base\View
 * @var $content string
 */
// $this->registerAssetBundle('app');
?>
<?php $this->beginPage(); ?>

<!DOCTYPE html>
<html lang="en">
  <head>
  <link href="favicon.ico" rel="shortcut icon" type="image/x-icon" />
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="ThemeFactory.net">

    <title>BEKTI UCIHA</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo $this->theme->baseUrl ?>/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php echo $this->theme->baseUrl ?>/css/jumbotron.css" rel="stylesheet">

  </head>

  <body>
    <?php $this->beginBody() ?>
    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">BEKTI UCIHA</a></a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <?php
  					echo Menu::widget([
  					  'options' => [
  					    "id"  => "nav",
  					    "class" => "nav navbar-nav"
  					  ],
					    'items' => [
					        ['label' => 'Home', 'url' => ['site/index']],
					        ['label' => 'About', 'url' => ['site/about']],
					        ['label' => 'Contact', 'url' => ['site/contact']],
					        ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
					    ],
	  				]);
		  		?>

        </div><!--/.navbar-collapse -->
      </div>
    </nav>

    <!-- Main jumbotron for a primary marketing message or call to action -->
     <nav class="jumbo-nav">
      <div class="navigation-wrapper">
        <div class="navigation">
          <div class="header-logo">
            <a href="#">
               <?php echo Html::img('@web/images/pokemon3.png') ?>
            </a>
          </div>
        </div>
      
        <!-- Example row of columns -->
        <div class="container">
          <div class="row">
            <div class="col-md-4">
              
             
              </div>
            </div>
          </div>
        </div>
        
      </div>
    </nav>

    <div class="container">
      <hr>
      
      <div class="row">
        <div class="col-md-12">
          <div class="enhanced">
              <?php echo $content; ?>
          </div>
        </div>
      </div>

      <footer>
        <p>&copy; <?php echo date("Y"); ?> Company, Inc.</p>
      </footer>
    </div> <!-- /container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="<?php echo $this->theme->baseUrl ?>/js/bootstrap.min.js"></script>
    <?php $this->endBody(); ?>
  </body>
</html>
<?php $this->endPage(); ?>
