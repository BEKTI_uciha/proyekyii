<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "data".
 *
 * @property integer $id
 * @property string $Nama_depan
 * @property string $Nama_Belakang
 * @property string $Alamat_Lengkap
 */
class Data extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'data';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'Nama_depan', 'Nama_Belakang', 'Alamat_Lengkap'], 'required'],
            [['id'], 'integer'],
            [['Nama_depan'], 'string', 'max' => 100],
            [['Nama_Belakang', 'Alamat_Lengkap'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'Nama_depan' => 'Nama Depan',
            'Nama_Belakang' => 'Nama  Belakang',
            'Alamat_Lengkap' => 'Alamat  Lengkap',
        ];
    }
}
